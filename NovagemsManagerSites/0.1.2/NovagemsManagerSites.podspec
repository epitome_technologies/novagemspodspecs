Pod::Spec.new do |s|
  s.name             = 'NovagemsManagerSites'
  s.version          = '0.1.2'
  s.summary          = 'A pod containing Sites module for NovagemsManager app.'

  s.description      = <<-DESC
A redistributable pod containing Sites module for NovagemsManager app.
                       DESC

  s.homepage         = 'https://bitbucket.org/epitome_technologies/manager_ios-sites'
  s.license          = 'MIT'
  s.author           = { 'Arsh Aulakh' => 'arsh@epitometechnologies.com' }
  s.source           = { :git => 'https://bitbucket.org/epitome_technologies/manager_ios-sites.git', :tag => s.version.to_s }

  s.ios.deployment_target = '9.0'

  s.source_files = 'NovagemsManagerSites/Classes/**/*.swift'
  
  s.resource_bundles = {
    'NovagemsManagerSites' => [
        'NovagemsManagerSites/Assets/*.xcassets',
        'NovagemsManagerSites/Classes/**/*.xib'
    ]
  }

  s.frameworks = 'UIKit', 'MapKit', 'CoreLocation'

  s.dependency 'Epilities'
  s.dependency 'Material'
  s.dependency 'TPKeyboardAvoiding'
  s.dependency 'DateToolsSwift'
  s.dependency 'Alamofire'
  s.dependency 'SwiftyJSON'
  s.dependency 'DZNEmptyDataSet'
  s.dependency 'DropDown'
  s.dependency 'Floaty'
  s.dependency 'SVProgressHUD'
  s.dependency 'SwipeCellKit'
  s.dependency 'Siesta'
  s.dependency 'Siesta/UI'
  s.dependency 'Siesta/Alamofire'
  s.dependency 'FSCalendar'
  s.dependency 'SwiftMessages'
  s.dependency 'AlamofireImage'
  s.dependency 'SDWebImage'
end
