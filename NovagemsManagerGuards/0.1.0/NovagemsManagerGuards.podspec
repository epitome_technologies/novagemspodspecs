Pod::Spec.new do |s|
  s.name             = 'NovagemsManagerGuards'
  s.version          = '0.1.0'
  s.summary          = 'A pod containing NovagemsManager app Guards module.'

  s.description      = <<-DESC
A redistributable pod containing NovagemsManager app Guards module.
                       DESC

  s.homepage         = 'https://bitbucket.org/epitome_technologies/manager_ios-guards'
  s.license          = 'MIT'
  s.author           = { 'Arsh Aulakh' => 'arsh@epitometechnologies.com' }
  s.source           = { :git => 'https://bitbucket.org/epitome_technologies/manager_ios-guards.git', :tag => s.version.to_s }

  s.ios.deployment_target = '9.0'

  s.source_files = 'NovagemsManagerGuards/Classes/**/*.swift'

  s.resource_bundles = {
        'NovagemsManagerGuards' => [
            'NovagemsManagerGuards/Assets/*.xcassets',
            'NovagemsManagerGuards/Classes/**/*.xib'
        ]
  }

  s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'Epilities'
  s.dependency 'Floaty'
  s.dependency 'Epilities'
  s.dependency 'SwipeCellKit'

end


